/*
 * xen_sme.h
 *
 * created on: Feb 23, 2017
 * author: aasthakm
 *
 * Similar to lsm_hooks.h
 * Provides LSM like generic interface to 
 * create pluggable function interfaces
 * 
 */

#ifndef __XEN_SME_H__
#define __XEN_SME_H__

#include <linux/skbuff.h>
#include <linux/netdevice.h>
#include "../bnx2x.h"
#include "sme_debug.h"

/*
 * Note: this does not implement the additional level 
 * of generic interfaces similar to the ones provided 
 * by security.c/h, which allow for multiple 
 * implementations of the LSM
 */

union sme_list_options {
  void (*update_cwnd) (struct sk_buff *skb, struct sock *sk, int caller,
      int b_cwnd, int b_pkts, int b_lost, int b_retrans, int b_sack,
      int a_cwnd, int a_pkts, int a_lost, int a_retrans, int a_sack,
      int una_diff, int delivered, int ack_flag, int num_marked_lost,
      char *extra_dbg_string);
  int (*intercept_select_queue) (struct net_device *dev, struct sk_buff *skb,
      char *extra_dbg_string);
  void (*intercept_tx_sent_queue) (struct netdev_queue *txq, unsigned int bytes);
  void (*intercept_tx_completed_queue) (struct netdev_queue *txq,
      unsigned int pkts, unsigned int bytes);
	int (*intercept_sk_buff) (struct sk_buff *skb, char *extra_dbg_string);
  void (*intercept_xmit_stop_queue) (struct net_device *dev,
      struct netdev_queue *txq, struct bnx2x_fp_txdata *txdata);
  int (*intercept_tx_int) (struct net_device *dev, struct bnx2x_fp_txdata *txdata);
  void (*intercept_free_tx_skbs_queue) (struct bnx2x_fastpath *fp);
	void (*parse_rx_data) (void *dev, int fp_idx, uint16_t rx_bd_prod,
			uint16_t rx_bd_cons, uint16_t comp_prod, uint16_t comp_cons,
			char *data, int data_len, char *extra_dbg_string);
	void (*print_sk_buff) (struct sk_buff *skb, struct sock *sk, char *extra_dbg_string);
	void (*intercept_rx_path) (void *dev, int fp_idx, uint16_t rx_bd_prod,
			uint16_t rx_bd_cons, uint16_t comp_prod, uint16_t comp_cons, int irq,
			char *extra_dbg_string);
	void (*print_rx_data) (void *dev, int fp_idx, uint16_t rx_bd_prod,
			uint16_t rx_bd_cons, uint16_t comp_prod, uint16_t comp_cons,
			char *extra_dbg_string);
};

struct sme_hook_heads {
  struct list_head update_cwnd;
  struct list_head intercept_select_queue;
  struct list_head intercept_tx_sent_queue;
  struct list_head intercept_tx_completed_queue;
	struct list_head intercept_sk_buff;
  struct list_head intercept_xmit_stop_queue;
  struct list_head intercept_tx_int;
  struct list_head intercept_free_tx_skbs_queue;
	struct list_head parse_rx_data;
	struct list_head print_sk_buff;
	struct list_head intercept_rx_path;
	struct list_head print_rx_data;
};

struct sme_hook_list {
	struct list_head list;
	struct list_head *head;
	union sme_list_options hook;
};

#define SME_HOOK_INIT(HEAD, HOOK)	\
{ .head = &xen_sme_hook_heads.HEAD, .hook = { . HEAD = HOOK } }

// increment this every time a new hook is added
#define NUM_XEN_SME_HOOKS 12

extern struct sme_hook_heads xen_sme_hook_heads;
extern struct sme_hook_list xen_sme_hooks[NUM_XEN_SME_HOOKS];

static inline void sme_add_hooks(struct sme_hook_list *hooks, int count)
{
	int i;
	for (i = 0; i < count; i++)
		list_add_tail_rcu(&hooks[i].list, hooks[i].head);
}

static inline void sme_delete_hooks(struct sme_hook_list *hooks, int count)
{
	int i;
	for (i = 0; i < count; i++)
		list_del_rcu(&hooks[i].list);
}

#endif /* __XEN_SME_H__ */
