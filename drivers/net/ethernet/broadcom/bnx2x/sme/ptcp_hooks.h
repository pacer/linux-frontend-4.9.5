/*
 * ptcp_hooks.h
 *
 * created on: Jun 21, 2018
 * author: aasthakm
 *
 * Similar to security.h/c
 * symbols exported to be used in TCP/IP
 * 
 */

#ifndef __PTCP_HOOKS_H__
#define __PTCP_HOOKS_H__

#include <linux/skbuff.h>
#include <linux/tcp.h>

#define DEFAULT_PTCP_MTU  1448

extern int PTCP_MTU;

void ptcp_print_sock_skb(struct sock *sk, struct sk_buff *skb, char *dbg_str);
int ptcp_rx_pred_flags(struct sock *sk, struct sk_buff *skb, struct tcphdr *th);
int ptcp_rx_handle_tcp_urg(struct sock *sk, struct sk_buff *skb, struct tcphdr *th);
int ptcp_rx_handle_dummy(struct sock *sk, struct sk_buff *skb);
int ptcp_rx_disable_coalesce(struct sock *sk, struct sk_buff *to,
    struct sk_buff *from);
int ptcp_rx_adjust_skb_size(struct sock *sk, struct sk_buff *skb, int req_len,
    int offset, int chunk, int copied, int flags);
int ptcp_rx_adjust_copied_seq(struct sock *sk, struct sk_buff *skb,
    int old_skb_len, int new_skb_len);

int ptcp_tx_adjust_seq(struct sock *sk, struct sk_buff *skb, int copy, int copied);
int ptcp_rxtx_sync_shared_seq(struct sock *sk, struct sk_buff *skb, int write);
int ptcp_tx_adjust_urg(struct sock *sk, struct sk_buff *skb);
int ptcp_tx_incr_tail(struct sock *sk, struct sk_buff *skb);
int ptcp_tx_incr_profile(struct sock *sk, struct sk_buff *skb);
int ptcp_is_paced_flow(struct sock *sk);
void ptcp_timer_rto_check(struct sock *sk);
int ptcp_get_retransmit_skb(struct sock *sk, struct sk_buff **skb_p);
int ptcp_build_rexmit_dummy(struct sock *sk, struct sk_buff **skb_p,
    uint32_t seqno, uint32_t seqack, int sacked_flags);

#endif /* __PTCP_HOOKS_H__ */
