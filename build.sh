#!/bin/bash

ncore=16
PWD=`pwd`
bnx_dir="$PWD/drivers/net/ethernet/broadcom/bnx2x"

date
make -j $ncore > make.out 2>&1
make modules -j $ncore >> make.out 2>&1
#make modules M=$bnx_dir -j $ncore >> make.out 2>&1
date
